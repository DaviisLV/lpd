﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SceletFallows : MonoBehaviour
{
    private bool isHit = false;
    public GameObject skeleton;
    public GameObject _sword;
    private GameObject _player;
    private NavMeshAgent _navMeshAgent;
     Animator animator;

    private AudioSource audioSorce;
    public AudioClip collect;


    [Range(0f, 50f)]
    [SerializeField]
    private float _followRadius = 10f;
 //   [Range(0f, 5f)]
   // [SerializeField]
    private float _attackRadius = 5f;

    private float _healt = 4f;


    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player"); 
        _navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        audioSorce = GetComponent<AudioSource>();
    }

    void Update()
    {
        

        if (Vector3.Distance(transform.position, _player.transform.position) < _followRadius) 
        {
            animator.SetBool("Idle", false);
            StartFallow();

            if (Vector3.Distance(transform.position, _player.transform.position) < _attackRadius)
            {

                animator.SetBool("Walk", false);
                animator.SetBool("Attack", true);
                
            }
            else
            {

                animator.SetBool("Walk", true);
                animator.SetBool("Attack", false);
            }
        }
        else
        {
            if (isHit == false)
           
            {
             
               animator.SetBool("Walk", false);
                animator.SetBool("Attack", false);
                animator.SetBool("Idle", true);
               
            }
            else
            {
                animator.SetBool("Idle", false);
                animator.SetBool("Walk", true);
            }

        }
     
    }


   
    IEnumerator Die()
    {
        StopFallow();
       animator.SetBool("Death", true);
        audioSorce.PlayOneShot(collect);
        BoxCollider[] myColliders = _sword.GetComponents<BoxCollider>();
        foreach (BoxCollider bc in myColliders) bc.enabled = false;
        CapsuleCollider[] S = skeleton.GetComponents<CapsuleCollider>();
        foreach (CapsuleCollider cc in S) cc.enabled = false;
       
        yield return new WaitForSeconds(3f); 
        Destroy(gameObject);
    }
    IEnumerator Damage()
    {
        
        animator.SetBool("Damage", true);  
        yield return new WaitForSeconds(0.25f);
        animator.SetBool("Damage", false);
    }

    public void RemoveHealth(float hitPower)
    {
        isHit = true;
        
        _healt -= hitPower;

        if (_healt < 0)
        {

            StartCoroutine(Die());       
        }
        else
        {
            StartFallow();
            StartCoroutine(Damage());
        }
     
    }

    private void StartFallow()
    {
        animator.SetBool("Walk", true);
        _navMeshAgent.SetDestination(_player.transform.position);
    }
  public void StopFallow()
    {
        Debug.Log(" Stop ");
        _navMeshAgent.isStopped = true;
    }
    public void StartAganeFallow()
    {
        Debug.Log(" Start ");
        _navMeshAgent.isStopped = false;
    }

    public void AttackSound()
    {
        if (audioSorce.isPlaying) { }
        else
        { audioSorce.PlayOneShot(collect); }
        
    }
}
