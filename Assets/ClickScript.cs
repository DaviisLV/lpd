﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickScript : MonoBehaviour {

    public AudioClip sound;
    private Button button;
    private AudioSource source;

	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
        source = GetComponent<AudioSource>();
        source.clip = sound;
        source.playOnAwake = false;
        button.onClick.AddListener(() => playSound());
	}
	
	// Update is called once per frame
	void playSound () {
        source.PlayOneShot(sound);
	}
}
