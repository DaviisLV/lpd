﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCollision : MonoBehaviour {
    public GameObject BulletBox;
    public Image p1;
    public Image p2;
    public Image p3;
    public Image p4;
    public Image p5;
    public Text colectText;
    private int _cauntItems = 0;
    private int _helth = 45;



     void OnControllerColliderHit(ControllerColliderHit hit)
    {

        if (hit.gameObject.tag == "ME")
        {
            Debug.Log(hit.gameObject.name.ToString());
           

            CollectBullets C = hit.gameObject.GetComponent<CollectBullets>();
            if (C == null) Debug.LogWarning("nav iznicinats");
            else { C.BulletsCollected();
                FireBullet.instace.bulletsLeft+=10;}
            

        }
        if (hit.gameObject.tag == "Sword")
        {
            Debug.LogError("Suka trapija");
            _helth -= 2;
            if (_helth <= 0)
            {
                StartCoroutine(FadeToLose());
                Debug.LogError("Nave mokas");
            }

            
        }
        if (hit.gameObject.tag == "CItems")
        {
            Destroy(hit.gameObject);
            _cauntItems++;
            Debug.Log(_cauntItems);
            switch (_cauntItems)
            {
                case 5:
                    p5.color = new Color32(0, 120, 25, 100);
                    StartCoroutine(FadeToWin());
                    break;
                case 4:
                    p4.color = new Color32(0, 120, 25, 100);
                    break;
                case 3:
                    p3.color = new Color32(0, 120, 25, 100);
                    break;
                case 2:
                    p2.color = new Color32(0, 120, 25, 100);
                    break;
                case 1:
                    p1.color = new Color32(0, 120, 25, 100);

                    break;
                default:
         
                    break;
            }   

        }


    }

       

    

    IEnumerator FadeToWin()
    {
    float fadeTime = GameObject.Find("FPSController").GetComponent<AutoFade>().BeginFade(1);
    yield return new WaitForSeconds(fadeTime);
    Application.LoadLevel(3);
    }
    IEnumerator FadeToLose()
    {
        float fadeTime = GameObject.Find("FPSController").GetComponent<AutoFadeDie>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        Application.LoadLevel(2);
    }
    // Use this for initialization
    void Start () {
    
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
