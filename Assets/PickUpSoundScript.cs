﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSoundScript : MonoBehaviour {

    private AudioSource audioSorce;
    public AudioClip collect;
    // Use this for initialization
    void Start () {
          audioSorce = GetComponent<AudioSource>();
    }
    public void PickUpBulletsSound()
    {
        audioSorce.PlayOneShot(collect);
    }
    
      
	
	// Update is called once per frame
	void Update () {
		
	}
}
