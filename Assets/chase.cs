﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class chase : MonoBehaviour {

    private GameObject _player;
    static Animator animator;
    private NavMeshAgent _navMeshAgent;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
         _navMeshAgent = GetComponent<NavMeshAgent>();
         _player = GameObject.FindGameObjectWithTag("Player");

    }
	
	// Update is called once per frame
	void Update () {

        Vector3 derection = _player.transform.position - transform.position;
        float angle = Vector3.Angle(derection, this.transform.forward);
        //čeko atalumu no speletaja
        // if (Vector3.Distance(transform.position, _player.transform.position) > closeEnoughDistance)
        if (Vector3.Distance(_player.transform.position, transform.position) < 60 && angle<135)
        {
          
          //  derection.y = 0;

            //this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(derection), 0.1f);

            animator.SetBool("Idle", false);

            if (derection.magnitude > 5)
            {

                _navMeshAgent.SetDestination(_player.transform.position);
                animator.SetBool("Walk", true);
                animator.SetBool("Attack", false);
            }
            else
            {
                animator.SetBool("Walk", false);
                animator.SetBool("Attack", true);
            }
        }
        else {
            animator.SetBool("Idle", true);
            animator.SetBool("Walk", false);
            animator.SetBool("Attack", false);
        }
	}
}
