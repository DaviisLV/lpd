﻿
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {


  //  public Button startButton;
  //  public Button exitButton;
 //   public Canvas dialog;

    // Use this for initialization
    void Start () {

        Screen.lockCursor = false;
      //  startButton = GetComponent<Button>();
      //  exitButton = GetComponent<Button>();
       // dialog = GetComponent<Canvas>();

    }
	

    public void StartGame()
    {
        StartCoroutine(FadeToNext());

    }

    IEnumerator FadeToNext()
    {
        float fadeTime = GameObject.Find("StartMenu").GetComponent<AutoFade>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        Application.LoadLevel(1);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
	
}
