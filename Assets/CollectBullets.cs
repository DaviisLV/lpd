﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectBullets : MonoBehaviour {

    private AudioSource audioSorce;
    public AudioClip collect;

    public void BulletsCollected()
    {
        Destroy(gameObject);
        // StartCoroutine(remove());
    }
    // Use this for initialization
    void Start()
    {
         audioSorce = GetComponent<AudioSource>();
    }


    IEnumerator remove()
    {
         audioSorce.PlayOneShot(collect);
         yield return new WaitForSeconds(1f);
            // Destroy(gameObject);
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
