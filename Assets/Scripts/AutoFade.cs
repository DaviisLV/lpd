﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using UnityEngine;


public class AutoFade : MonoBehaviour
{
    public Texture2D fadeout;
    public float fadeSpeed = 0.8f;

    private int drawOrder = -1000;
    private float alfa = 1f;
    private int fadeDir = -1;

    private void OnGUI()
    {
        alfa += fadeDir * fadeSpeed * Time.deltaTime;

        alfa = Mathf.Clamp01(alfa);

        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alfa);
        GUI.depth = drawOrder;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeout);

    }

    public float BeginFade(int dir)
    {
        fadeDir = dir;
        return (fadeSpeed);
    }

    private void OnLevelWasLoaded(int level)
    {
        BeginFade(-1);
    }
}