﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireBullet : MonoBehaviour {
    public static FireBullet instace;
    public GameObject Bullet_StartPoint;

    //Drag in the Bullet Prefab from the Component Inspector.
    public GameObject Bullet;

    //Enter the Speed of the Bullet from the Component Inspector.
    public float Bullet_Forward_Force;

    private AudioSource audioSorce;
    public AudioClip shootSound;
    public AudioClip emtyClick;
    public AudioClip ReloadSound;

    public int bulletsPerMag = 10;
    public int bulletsLeft = 20;
    public int bulletsInMag;
    public Text BulletsLeft;
    public Text BulletsInMag;
    private Animator anime;

    public ParticleSystem muzzleFlesh;

    public float fireRate = 0.1f;
    float fireTimer;

    private bool isReloading;

    

    // Use this for initialization
    void Start()
    {
        anime = GetComponent<Animator>();
        audioSorce = GetComponent<AudioSource>();
        bulletsInMag = bulletsPerMag;
        instace = this;

    }

    // Update is called once per frame
    void Update()
    {
        
        BulletsLeft.text = "Total: "+(bulletsLeft + bulletsInMag).ToString();
        BulletsInMag.text = "In Mag: "+bulletsInMag.ToString();

        if (Input.GetKey(KeyCode.Mouse0))

        {
            if (bulletsInMag > 0)
            {
                Fire();
            }
            else if (bulletsLeft > 0)
            {
                DoReload();
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                    PlayEmtyClick();
            }

        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (bulletsInMag < bulletsPerMag && bulletsLeft > 0)
                DoReload();
        }
        if (fireTimer < fireRate)
        {
            fireTimer += Time.deltaTime;
        }

    }
    private void FixedUpdate()
    {
        AnimatorStateInfo info = anime.GetCurrentAnimatorStateInfo(0);
        
        isReloading = info.IsName("Reload");
    }
    private void Fire()
    {
        if (fireTimer < fireRate || bulletsInMag <= 0 || isReloading) return;

        //The Bullet instantiation happens here.
        GameObject Temporary_Bullet_Handler;
        Temporary_Bullet_Handler = Instantiate(Bullet, Bullet_StartPoint.transform.position, Bullet_StartPoint.transform.rotation) as GameObject;

        //Sometimes bullets may appear rotated incorrectly due to the way its pivot was set from the original modeling package.
        //This is EASILY corrected here, you might have to rotate it from a different axis and or angle based on your particular mesh.
        Temporary_Bullet_Handler.transform.Rotate(Vector3.left * 180);

        //Retrieve the Rigidbody component from the instantiated Bullet and control it.
        Rigidbody Temporary_RigidBody;
        Temporary_RigidBody = Temporary_Bullet_Handler.GetComponent<Rigidbody>();

        //Tell the bullet to be "pushed" forward by an amount set by Bullet_Forward_Force.
        Temporary_RigidBody.AddForce(transform.forward * Bullet_Forward_Force);

        //Basic Clean Up, set the Bullets to self destruct after 10 Seconds, I am being VERY generous here, normally 3 seconds is plenty.
        Destroy(Temporary_Bullet_Handler, 10.0f);
     

        anime.CrossFadeInFixedTime("Fire", 0.1f);
        muzzleFlesh.Play();
        PlayShootSound();
        bulletsInMag--;
        fireTimer = 0.0f;
    }

    public void Reload()
    {
        PlayReloadSound();
        if (bulletsLeft <= 0) return;

        int bulletsToLoad = bulletsPerMag - bulletsInMag;
        int bulletsToDeduct = (bulletsLeft >= bulletsToLoad) ? bulletsToLoad : bulletsLeft;

        bulletsLeft -= bulletsToDeduct;
        bulletsInMag += bulletsToDeduct;
    }

    private void DoReload()
    {
    

        if (isReloading) return;

        anime.CrossFadeInFixedTime("Reload",0.01f);
    }

    private void PlayShootSound()
    {
        audioSorce.PlayOneShot(shootSound);
    }
    private void PlayEmtyClick()
    {
        audioSorce.PlayOneShot(emtyClick);
    }
    private void PlayReloadSound()
    {
        audioSorce.PlayOneShot(ReloadSound);

    }


    void OnControllerColliderHit(ControllerColliderHit hit)
    {
      if (hit.gameObject.name == "aaa")
        {
           // Debug.Log("Pieskaros");

        }
        else
        {
          
        }
    }

}

